# Script om excel bestand te lezen met Clienten per type maatwerkarrangement

library(readxl)

Voeg_WmoSubcategorieen_toe <- function(df, jaren=2017:2020) {
  "
  Deze functie laadt de WmoSubcategorieën voor de jaren 2017 t/m 2019;
  en voegt deze toe aan de bestaande df op gemeenteniveau. Het toevoegen
  gebeurt op basis van gwb_code en jaartal
  
  Parameters:
    df: dataframe waaraan de Wmo-subcategorieën worden toegevoegd.
    
  Returns:
    dataframe met daaraan Wmo-subcategorieën toegevoegd.
  "
  # definieer het pad + bestandsnaam om later te gebruiken
  full.file.name <- file.path(
    external_data_dir, 
    "Wmo clienten per type maatwerkarrangement 2017-2019.xlsx"
  )
  
  excel_jaren <- 2017:2019
  
  Wmo_kolomnamen <- c(
    "Totaal",
    "Ondersteuning_thuis_totaal",
    "Begeleiding",
    "Persoonlijke_verzorging",
    "Kortdurend_verblijf",
    "Ov._ondersteuning_individu_of_huishouden",
    "Dagbesteding",
    "Overige_groepsgerichte_ondersteuning",
    "Overige_maatwerkarrangementen",
    "Hulp_bij_het_huishouden",
    "Verblijf_en_opvang_totaal",
    "Beschermd_wonen",
    "Opvang",
    "Spoedopvang",
    "Overige_beschermd_wonen_en_opvang",
    "Hulpmiddelen_en_diensten_totaal",
    "Woondiensten",
    "Vervoersdiensten",
    "Rolstoelen",
    "Vervoervoorzieningen",
    "Woonvoorzieningen",
    "Overige_hulpmiddelen"  
  )
  kolomnamen <- c(
    "Gemeentenaam",
    "Gemeentecode",
    "skip",
    Wmo_kolomnamen
  )
  
  wmo_sub <- data.frame()
  
  for (jaar in intersect(excel_jaren, jaren)) {
    # bereken het tabel nummer
    tabel_nr <- jaar - min(jaren) + 1
    
    # lees de excel in - 1 sheet per keer
    df_tabel <- suppressMessages(
      read_excel(
        full.file.name, 
        sheet=sprintf("Tabel %d", tabel_nr), 
        skip=10,
        col_names=FALSE,
        na="."
      )
    )
    
    # Neem alleen absolute aantallen mee
    df_tabel <- df_tabel[,1:length(kolomnamen)]
    
    # wijzig de kolomnamen
    colnames(df_tabel) <- kolomnamen
    
    # verwijder skip en voeg jaar toe en voeg aan output toe
    wmo_sub <- rbind(
      wmo_sub, 
      df_tabel %>%
       # dplyr::select(all_of("skip"), all_of("Gemeentenaam")) %>%
        select(-skip, -Gemeentenaam) %>%
        mutate(jaar=!!jaar)  
    )
  }
  
  #voeg verbetering toe
  # library(cbsodataR)
  # library(dplyr)
  # library(readr)
  # library(fs)
  # 
  # # Function to check and download data
  # check_and_download <- function(id, catalog, filename) {
  #   if (!file_exists(filename)) {
  #     data <- cbs_get_data(
  #       id = id,
  #       catalog = catalog,
  #       Perioden = sprintf("%dJJ00", jaren[jaren>=2020])
  #     )
  #     write_csv(data, filename)
  #     return(data)
  #   } else {
  #     return(read_csv(filename))
  #   }
  # }
  # 
  # # Download or load vertaaltabel
  # vertaaltabel_filename <- "vertaaltabel.csv"
  # if (!file_exists(vertaaltabel_filename)) {
  #   vertaaltabel <- cbs_get_meta(
  #     id = "84839NED", 
  #     catalog = "CBS"
  #   )[["TypeMaatwerkvoorziening"]][c("Key", "Title")] %>%
  #     mutate(Title = gsub(",", "", gsub(" ", "_", Title)))
  #   write_csv(vertaaltabel, vertaaltabel_filename)
  # } else {
  #   vertaaltabel <- read_csv(vertaaltabel_filename)
  # }
  # 
  # # Download or load wmo_sub_2020 data
  # wmo_sub_2020_filename <- "wmo_sub_2020.csv"
  # wmo_sub_2020 <- check_and_download("84839NED", "CBS", wmo_sub_2020_filename)
  # ===============================
  # voeg 2020 en later toe
  #gewijzigd TypeMaatwerkarrangement
  # laad de vertaal tabel
  vertaaltabel <- cbs_get_meta(
    id="84839NED", 
    catalog="CBS"
  )[["TypeMaatwerkvoorziening"]][c("Key", "Title")] %>%
    mutate(Title = gsub(",", "", gsub(" ", "_", Title)))
  
  # haal de 2020 wmo-sub data op
  wmo_sub_2020 <- cbs_get_data(
    id="84839NED",
    catalog="CBS",
    Perioden = sprintf("%dJJ00", jaren[jaren>=2020])
  )
  
  # zet om naar zinnige kolomnamen gewijzigd, TypeMaatwerkarrangement
  wmo_sub_2020 <- wmo_sub_2020 %>%
    mutate(
      RegioS = trimws(RegioS),
      TypeMaatwerkvoorziening = vertaaltabel$Title[
        match(TypeMaatwerkvoorziening, vertaaltabel$Key)
      ],
      jaar=as.numeric(substr(Perioden, 1, 4))) %>%
    select(-WmoClientenPer1000Inwoners_2, -Perioden) %>%
    pivot_wider(
      names_from = TypeMaatwerkvoorziening, 
      values_from = WmoClienten_1, 
      names_glue = "{TypeMaatwerkvoorziening}"
    ) %>%
    rename(Gemeentecode=RegioS)
  
  # selecteer alleen de kolommen die ook in de wmo_sub aanwezig zijn
  # bv "generieke toewijzing" staat niet in wmo_sub (afkomstig uit de excel)
  # maar wel op statline
  wmo_sub_2020 <- wmo_sub_2020[,intersect(names(wmo_sub), names(wmo_sub_2020))]
  
  # verbind samen
  wmo_sub <- as.data.frame(
    rbindlist(list(wmo_sub, wmo_sub_2020), use.names=T)
  )
  
  
  # bereken merge met data-frame
  out <- merge(
    df,
    wmo_sub,
    by.x = c("wijkcode", "jaar"),
    by.y = c("Gemeentecode", "jaar"),
    all.x=TRUE
  )
  
  # bereken aandelen
  for (kolom in Wmo_kolomnamen) {
    out[,make.names(paste0("percentage_wmo_sub_", kolom))] <- 100 * out[,kolom] / out[,"aantal_inwoners"]
  }  
  
  # verwijder aantallen
  out <- out %>% select(-all_of(Wmo_kolomnamen))
  
  return(out)
}


