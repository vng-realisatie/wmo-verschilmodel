# Config bestand


# Plek waar de gecreëerde datasets komen te staan
data_dir <- file.path("data")

# Plek waar de processed datasets komen te staan
processed_data_dir <- file.path(data_dir, "processed")

# Plek waar de oorspronkelijke datasets komen te staan
raw_data_dir <- file.path(data_dir, "raw")

# Plek waar extra informatie over de (externe) data komt te staan
external_data_dir <- file.path(data_dir, "external")

# Plek waar de gecreeerde modellen komen te staan
model_dir <- file.path("models")

# Plek waar de rapportages, zoals betrouwbaarheidsmetrieken, komen te staan
rapportage_dir <- file.path("reports")

# Plek waar de code komt te staan
scripts_dir <- file.path("src")

# Plek waar de code voor het dashboard staat
dashboard_dir <- file.path("dashboard")

# Datapreparatie
dataprep_dir <- file.path(scripts_dir, "data")

# Scripts om de features te selecteren
feature_dir <- file.path(scripts_dir, "features")

# Scripts om de modellen te trainen
modeltrain_dir <- file.path(scripts_dir, "models")

# Scripts om naar de toekomst te voorspellen
voorspelling_dir <- file.path(scripts_dir, "forecast")

# Scripts voor algemene configuraties en handige functies
util_dir <- file.path(scripts_dir, "utils")

# Scripts om de resultaten te visualiseren
visualisatie_dir <- file.path(scripts_dir, "visualization")

# Plek om de figuren op te staal
figuur_dir <- file.path(rapportage_dir, "figures")


# CBS Ids voor de kwb en wmo data
#lijst is aangevuld tot '23'
CBS_IDS <- data.frame(
  jaar = 2013:2023,
  
  kwb = c('82339NED', '82931NED', '83220NED', '83487NED', '83765NED', '84286NED', '84583NED', "84799NED", "85039NED", "85318NED","85618NED"),
  wmo = c(NA, NA, NA, "83620NED", '84751NED', '84752NED', '84753NED', '84908NED', '85047NED', '85377NED','85639NED'),
  ses = c(NA, NA, NA, NA, NA, NA, NA, NA, "85163NED", "85539NED","85900NED"),
  nabijheid = c('82341NED', '82829NED', '83304NED', '83749NED', '84334NED', '84463NED', '84718NED', '84953NED', '85231NED', '85560NED', '85560NED')
)


# urls voor indicator Hoogopgeleiden clo.nl
#CLO_basedir = 'https://www.clo.nl/sites/default/files/datasets/'
# CLO_basedir = 'https://data.pbl.nl/api/content/v2/infographics/downloads/'
# CLO_filename <- data.frame(
#   jaartal = 2011:2021,
#   bestandsnaam = c(
#     '2100_002k_clo_03_nl.xlsx',
#     '2100_001k_clo_04_nl.xlsx',
#     '2100_001k_clo_05_nl.xlsx',
#     '2100_001k_clo_06_nl.xlsx',
#     '2100_001k_clo_07_nl.xlsx',
#     '2100_001k_clo_08_nl.xlsx',
#     '2100_001k_clo_09_nl.xlsx',
#     '2100_001k_clo_10_nl.xlsx',
#     '2100_001k_clo_11_nl.xlsx',
#     '2100_001k_clo_12_nl.xlsx',
#     '2100_001k_clo_13_nl.xlsx'
#   )
# )

# 100724
# Hoogopgeleiden data wordt opgehaald maar niet gebruikt
# Uitzoeken of we het gaan gebruiken of er uit halen
CLO_basedir = 'https://data.pbl.nl/api/content/v2/infographics/downloads/'
#waar wordt dit voor gebruikt? inkijken naar het gebruik ervan

#https://data.pbl.nl/api/content/v2/infographics/downloads/2100_001k_clo_09_nl.xlsx
#https://data.pbl.nl/api/content/v2/infographics/downloads/2100_001k_clo_15_nl.xlsx
CLO_filename <- data.frame(
  jaartal = 2011:2022,
  #oud 'c-2100-002k-clo-03-nl.xls'
  #nieuw '2100_002k_clo_03_nl.xlsx'
  bestandsnaam = c(
    '2100_002k_clo_03_nl.xlsx',
    '2100_001k_clo_04_nl.xlsx',
    '2100_001k_clo_05_nl.xlsx',
    '2100_001k_clo_06_nl.xlsx',
    '2100_001k_clo_07_nl.xlsx',
    '2100_001k_clo_08_nl.xlsx',
    '2100_001k_clo_09_nl.xlsx',
    '2100_001k_clo_10_nl.xlsx',
    '2100_001k_clo_11_nl.xlsx',
    '2100_001k_clo_12_nl.xlsx',
    '2100_001k_clo_13_nl.xlsx',
    '2100_001k_clo_14_nl.xlsx'
  )
)

#deze halte ids komen van: https://transitfeeds.com/p/ov/814
#de id is eigenlijk een date format van de file die we nemen; voor 2023 is de url naar de data bijvoorbeeld:
#https://transitfeeds.com/p/ov/814/20230405-2 <-- daarvan nemen we het laatste deel
#we kiezen nu voor een peildatum begin april 
OV_HALTE_URLS <- data.frame(
  jaar = 2017:2023,
  id = c('20170403', '20180405-2', '20190420', '20200401', '20210403', '20220404', '20230405-2')
)

# vertaaltabel code stedelijkheid
vertaaltabel_stedelijkheid = c(
  "Zeer sterk stedelijk",
  "Sterk stedelijk",
  "Matig stedelijk",
  "Weinig stedelijk",
  "Niet stedelijk"
)
names(vertaaltabel_stedelijkheid) <- 1:5
