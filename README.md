# Wmo verschilmodel
==============================

Deze repository bevat de code voor het Wmo verschilmodel.

Deze repo bevat de scripts die een model trainen om het Wmo-gebruik in de toekomst te voorspellen. Wmo staat voor Wet maatschappelijke ondersteuning. Het is oorspronkelijk begonnen in de gemeente Den Haag, en vervolgens overhandigd naar de VNG (Vereniging Nederlandse Gemeenten) om te kijken naar opties voor opschaling naar heel Nederland. Het werk aan de code bestaat nu uit het naar productie brengen en periodiek updaten van de databronnen ervan opdat de voorspellingen verbeteren en er op vertrouwd kan worden door de gemeenten. Er is/wordt aan gewerkt door o.a. data scientisten van VNG en van de gemeenten Den Haag, Rotterdam, Velsen, Amersfoort en Weert.

Daarnaast bevat deze repo ook code om de resultaten van het model weer te geven in een R Shiny dashboard.

## Branches / Aftakkingen

Deze repository is opgezet in 3 branches. De main is het werkende voorspelmodel, op de develop bracnh/tak wordt het project vernieuwd. De branch/tak archief bevat het eerste opzet van de Wmo voorspelmodel.

Voor suggesties omtrent de code of het project, neem gerust contact op met wmovoorspelmodel@vng.nl

## Inhoud
Deze repository is opgezet volgens de principes van 
<a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience

De data wordt in principe geladen door gebruik te maken van openbare API's. 
Na de initiële download wordt de data lokaal in de _data_ folder geplaatst

Naast dat nog gewerkt wordt aan het volgen van de best practices geldt al wel dat:
- alle scripts die deel zijn van de voorspelling zijn te vinden in de folder _src_;
- alle scripts die deel uitmaken van het dashboard staan in de folder _dashboard_;

## Conventies

Voor de variabelen en kolomnamen in de tabellen worden betekenisvolle namen toegekend. Bijvoorbeeld:
* aantal_bijstand voor het aantal mensen dat een bijstandsuitkering ontvangt;
* aandeel_bijstand voor de fractie mensen dat een bijstandsuitkering ontvangt ten opzichte van het totaal aantal mensen;
* perc_bijstand voor het percentage mensen dat een bijstandsuitkering ontvangt ten opzichte van het totaal aantal mensen.

De functies en werking van de code wordt duidelijk uitgelegd in commentaar. De voertaal daarbij is Nederlands.
Voor specifieke bewerkingen of concepten waarvoor geen gangbare Nederlandse term bestaat, mag Engels gebruikt worden.

Het implementeren van een specifiek element van de code of het oplossen van een probleem of fout gebeurt in een aparte branch.
Zodra het werk in deze branch is afgerond dient een merge-request te worden ingediend. 
Een collega controleert het werk en de invloed hiervan op de hoofdcode.
Als dit goed bevonden is kan de branch gemerged worden met de develop-branch.

Naast de develop branch is er ook een master branch. De master branch wordt periodiek ge-updated vanuit de develop branch.

## Uitvoeren code

### Opzetten project
Zorg dat het Rproject behorend bij deze repository wordt geladen. Als de repository in RStudio via `File > New Project > Version Control` is aangemaakt, gaat dit automatisch goed. Als de repository via een zip-file is gedownload van Gitlab, open dan het wmo-verschilmodel.Rproj file handmatig. _Als dit niet wordt gedaan, ontstaan problemen met sommige packages, zoals grenswijzen_.

We gebruiken Renv voor het versiebeheer van de packages en de R-versie. Om direct te kunnen beginnen installeer je eerst Renv via `install.packages('renv')` en daarna alle overige relevante packages met het commando `renv::restore()`.

Vervolgens kun je het script _mainscript_ aftrappen in de folder _src_. Hieronder staan de parameters gedefinieerd, zoals voor welke jaren er al data is en hoe ver vooruit voorspeld dient te worden. Vervolgens wordt _full_pipeline_function_ aangeroepen. Dit zorgt ervoor dat alle andere scripts in de goede volgorde worden gerunt. Daarover hieronder meer.

**Mogelijke problemen**

Mocht je onverhoed toch errors tegenkomen, hieronder staan een paar bekende met bijbehorende oplossingen:

- Het ogenschijnlijk vastlopen van Rstudio bij het runnen van code -> run `remove.packages("Rcpp")`, start Rstudio opnieuw op, en run vervolgens `install.packages("Rcpp")`. Nu werkt het weer.

- Error bij het installeren van package Matrix -> `remove.packages('Matrix')` en vervolgens `install.packages('https://cran.r-project.org/src/contrib/Archive/Matrix/Matrix_1.3-2.tar.gz', repos = NULL, type = 'source')`

- Error bij het installeren van een package (bijv. MASS of rlang) met de waarschuwing: `'make' not found` -> volg de eerste 2 stappen van [deze website van Rtools](https://cran.r-project.org/bin/windows/Rtools/), i.e. installeer het juiste .exe-bestand voor jouw pc, en maak dan het `.Renviron` document aan (zorg ervoor dat het `.txt`-document meer is!!), waarin je `PATH="${RTOOLS40_HOME}\usr\bin;${PATH}"` zet (bijv. door het te openen met Notepad/Kladblok).

- Error bij het draaien van het mainscript met grenswijzigingen. Het _grenswijzigen_ package geeft aan dat bepaalde matrices niet gevonden kunnen worden. De oorzaak is waarschijnlijk dat de renv::restore() niet is uitgevoerd in het Rproj at bij deze repository hoort. Zie opzetten van project voor de oplossingsrichting.

- Bij problemen met mran is het mogelijk om mran voor renv het gebruiken van de mran repositories uit te zetten met het volgende commando: 
`options(renv.config.mran.enabled = FALSE)`

### Trainen en toetsen modellen

Het trainen en toetsen van voorspelmodellen kan worden uitgevoerd vanuit het hoofdscript _mainscript_. Dit script roept via _full_pipeline_function_ diverse scripts en functies aan voor de uitvoer van specifieke taken. Bij de eerste keer dat het script wordt uitgevoerd, zullen diverse data bestanden nog niet aanwezig zijn. Het script is zo opgezet, dat deze data door middel van api's van het CBS wordt opgehaald en lokaal wordt neergezet in de _data_ folder van dit R-project. Bij een volgende keer dat het script wordt uitgevoerd kunnen deze lokale kopieën worden gebruikt wat tot aanzienlijke snelheidswinst leidt. 

Naast de standaard instellingen kun je in dit script ook een aantal variabelen naar wens veranderen om zo de uitkomst te vormen naar de behoefte. Deze staan bovenaan in het script.


### Toevoegen lokale data prognoses

Standaard maakt het Wmo-voorspelmodel gebruik van:
- lineaire extrapolatie voor alle betreffende indicatoren
- de CBS bevolkingsprognose voor het aantal inwoners op gemeenteniveau. Deze 
  prognose wordt naar rato omgeslagen naar wijkniveau.

Vaak is binnen de gemeente meer inzicht in de demografische ontwikkelingen
in de wijken dan op landelijk niveau beschikbaar is. Het script `src\features\toevoegen_lokale_prognose.R` 
staat toe deze lokale inzichten te verwerken in de voorspellingen. 

De functies in dit script lezen daartoe een csv-bestand in dat door de gemeenten
zelf geprepareerd moet worden. Er zijn twee voorbeeld csv-bestanden beschikbaar in
de folder `data/external`. Deze csv-bestanden moeten tenminste de kolommen _gwb_code_,
_jaar_ en _regionaalniveau_ bevatten. 
Daarnaast moet het csv-bestand de kolommen bevatten die overschreven moeten 
worden door de lokale prognose. 

Er zijn twee voorbeeld bestanden aanwezig. Het voorbeeldbestand 
`lokale_data_voorbeeld_DenBosch.csv` geeft alternatieve lokale data weer voor de 
gemeente 's-Hertogenbosch en twee wijken in deze gemeente. Dit type bestand kan
gebruikt worden indien er voor de analytische methode voor de berekening van 
betrouwbaarheidsmarges wordt gekozen.

Indien er gekozen wordt voor de _sampling_ methode, moeten ook de varianties
van de lokale prognoses van de indicatoren gegeven worden. Een voorbeeld staat
in het bestand `lokale_data_voorbeeld_DenBosch_sampling.csv`. In dit geval 
wordt de 'sampling' methode wordt gebruikt voor het berekenen van de 
betrouwbaarheidsmarges.

De bestandsnaam en de opties voor het berekenen van de betrouwbaarheidsmarges
kunnen worden ingesteld bovenaan in _mainscript.R_.

### Toevoegen van nieuw jaar
Data voor een nieuw jaar moeten worden ingelezen. Voor sommige datasets van CBS wordt per jaar een nieuw ID uitgegeven. Deze moeten hardcoded toegevoegd worden in `src/config.R`:

```
CBS_IDS <- data.frame(
  jaar = 2013:2021,
  kwb = c('82339NED', '82931NED', '83220NED', '83487NED', '83765NED', '84286NED', '84583NED', "84799NED", "85039NED"),
  wmo = c(NA, NA, NA, NA, '84751NED', '84752NED', '84753NED', '84908NED', '85047NED')
)

```
Pas in bovenstaande de jaar-range aan en vul voor kwb en wmo de nieuwe id's toe aan de lijst.

In het mainscript (`src/mainscript.R`) moeten de volgende parameters aangepast worden:

```
# Forecast parameters
jaar_begin_historie <- 2017
jaar_eind_historie <- 2021
peiljaar <- 2021  # jaar voor de naamgeving van wijken
jaar_begin_voorspelling <- 2022
jaar_eind_voorspelling <- 2026

```

Indien het dashboard ook bijgewerkt moet worden, moeten in `dashboard/config_dashboard.R` de volgende parameters aangepast worden:

```
# Peiljaar is het jaar waarvandaan de voorspelling begint
peiljaar <- 2021

# minimum en maximumjaar die gevisualiseerd worden
min_jaar <- 2017
max_jaar <- 2026
```

Een aantal (nieuwe) wijken hebben mogelijk geen inwoners. Het draaien van het mainscript geeft dan een foutmelding. In `src/utils/util_functions.R` in de wrapper_grenswijziging functie is dat op te lossen door deze wijken niet mee te nemen voor het peiljaar.

Daarnaast wil je waarschijnlijk ook de nieuwe grenzen meenemen. Zorg er daarom voor dat uit de map data de bestanden `grensindeling_wijk.rds` en `grensindeling_gemeente.rds` verwijderd zijn. Kopieer na afloop de nieuwe bestanden ook naar het dashboard, als je daar de nieuwe grensindelingen wilt gebruiken.

### Opstarten van het dashboard

De code voor het dashboard staat in de folder _dashboard_. Het hoofdscript in deze folder betreft _app.R_. Door dit script uit te voeren, kan het dashboard lokaal worden opgestart. Het hoofdscript maakt gebruik van een aantal hulpscripts. In het script _ui.R_ staat de gebruikersinterface van het dashboard gespecificeerd. Voor het uitrekenen van een aantal scenario's zijn extra scripts uit de hoofdcode nodig. Om deze toegankelijk te maken voor het dashboard, worden deze gekopieerd uit de hoofdcode naar deze folder. Het betreft de scripts _pas_model_toe.R_ en _util_verschil_functies.R_. Het wordt aanbevolen deze laatste scripts enkel in de hoofdcode aan te passen en niet in de dashboard-folder. 

## Contact
Neem contact met ons op in geval van vragen of opmerkingen.

Voor technische vragen en/of project-gerelateerde vragen, mail naar wmovoorspelmodel@vng.nl

## Licentie

<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl"><img alt="Creative Commons-Licentie" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dit werk valt onder een <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl">Creative Commons Naamsvermelding-NietCommercieel-GelijkDelen 4.0 Internationaal-licentie</a>.

Project Organization
------------

    ├── LICENSE
    ├── DESCRIPTION
    ├── README.md          <- De top-level README voor ontwikkelaars die gebruik maken van het project.
    ├── dasboard
    │   ├── data           <- Data nodig voor het dashboard.
    │   ├── models         <- Getrainde voorspelmodellen nodig voor het dashboard.
    │   └── rsconnect      <- Folders en bestanden om het dashboard on shinyapps.io te laten functioneren.
    │
    ├── data
    │   ├── external       <- Data van derden.
    │   ├── interim        <- Tussentijdse data die reeds is aangepast.
    │   ├── processed      <- De uiteindelijke dataset voor het modelleren.
    │   └── raw            <- De originele, onveranderlijke datasets (dat wat we niet kunnen krijgen via API's.
    │
    ├── models             <- De getrainde modellen, model voorspellingen en model definities.
    │
    ├── references         <- Data woordenboeken, handleidingen, en verder verklarend materiaal.
    │
    ├── reports            <- Gegenereerde analyses, bijv. HTML, PDF, LaTeX, etc.
    │   └── figures        <- Gegenereerde grafieken en figuren voor in rapportages.
    │
    ├── src                <- Broncode van dit project.
    │   ├── data           <- Scripts voor het downloaden, genereren en bewerken van data.
    │   │   ├── AdresDataOphalen.R 
    │   │   ├── Bevolkinsprognose.R 
    │   │   ├── DataOphalen.R 
    │   │   ├── DataOpslaan.R 
    │   │   └── DataPreparatie.R
    │   │
    │   ├── features       <- Scripts voor het omzetten van de data in features voor de modellen.
    │   │   ├── config_grenswijzigingen.R 
    │   │   ├── extrapoleer_functies.R 
    │   │   ├── toevoegen_lokale_prognose.R   <- Script om lokale prognose toe te voegen 
    │   │   └── util_verschil_functies.R
    │   │
    │   ├── models         <- Scripts voor het trainen van modellen
    │   │   ├── maak_model.R 
    │   │   └── pas_model_toe.R
    │   │
    │   ├── utils          <- Scripts die helpen bij diversie taken 
    │   │   ├── aantal_naar_aandeel_config.json 
    │   │   ├── pring_debug_info.R 
    │   │   └── util_functions.R
    │   │
    │   ├── config.R                    <- Script met technische definities en configuraties
    │   ├── libraries.R                 <- Script met benodigde bibliotheken
    │   ├── full_pipeline_function.R    <- Script met daarin de hoofdfunctie het runnen van de pipeline
    │   └── mainscript.R                <- Hoofdscript dat alle variabelen juist zet en de full_pipeline_function aanroept
    │       
    └── renv               <- Alle benodigde packages in de juiste versies, worden niet gepushed naar git.
    

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

