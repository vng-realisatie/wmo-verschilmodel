# Om full_pipeline_function te sourcen moet de working directory goed staan,
# maar om de test uit te voeren moet de working directory weer teruggezet worden
# naar het originele pad.
origineel_pad <- getwd()
# NOT_CRAN wordt op 'true' gezet, zodat onderstaande test niet wordt geskipt in de gitlab-ci.
Sys.setenv("NOT_CRAN" = "true")

test_that('Output Wmo-totaal blijft gelijk met analytische onzekerheid', {
  setwd('../..')
  # Als de variable NOT_CRAN toch "false" is, skip dan de test maar voordat ie helemaal runt
  # Anders wordt ie geskipt na het runnen, en dat is zonde van de tijd.
  skip_on_cran()
  
  source('src/full_pipeline_function.R')
  # test of de bestanden van Wmo totaal op zowel wijkniveau als
  # gemeenteniveau gelijk blijven of dat er veranderingen optreden.
  df.categorieen <- data.frame(
    categorie = rep('percentage_wmo_totaal', 2),
    niveau = c('wijk', 'gemeente')
  )
  # Roep de gehele pipeline aan
  wmo_voorspellen(jaar_begin_historie = 2017,
                  jaar_eind_historie = 2020,
                  df.categorieen = df.categorieen,
                  model_data_cache = F,
                  model.grenswijzigingen = "model.1",
                  PI_analytisch = T,
                  min_gebruikers = 100,
                  voor_dashboard = F
                )
  # Zet het pad weer terug naar de originele working directory
  setwd(origineel_pad)
  expect_snapshot_file(path = file.path('..', '..', 'data', 'final','uitkomst_percentage_wmo_totaal_gemeente.csv'),
                       name = 'analytisch_gemeente_totaal.csv')
  expect_snapshot_file(path = file.path('..', '..', 'data', 'final','uitkomst_percentage_wmo_totaal_wijk.csv'),
                       name = 'analytisch_wijk_totaal.csv')
})

test_that('Output Wmo-totaal blijft gelijk met gesimuleerde onzekerheid', {
  setwd('../..')
  # Als de variable NOT_CRAN toch "false" is, skip dan de test maar voordat ie helemaal runt
  # Anders wordt ie geskipt na het runnen, en dat is zonde van de tijd.
  skip_on_cran()

  source('src/full_pipeline_function.R')
  # test of de bestanden van Wmo totaal op gemeenteniveau gelijk blijven of dat
  # er veranderingen optreden.
  # Er kan worden beargumenteerd dat de code gelijk is voor wijkniveau en gemeenteniveau
  # bij het simuleren, waardoor maar 1 getest hoeft te worden.
  df.categorieen <- data.frame(
    categorie = 'percentage_wmo_oh',
    niveau = 'gemeente'
  )
  set.seed(1)
  # Roep de gehele pipeline aan
  wmo_voorspellen(jaar_begin_historie = 2017,
                  jaar_eind_historie = 2020,
                  df.categorieen = df.categorieen,
                  model_data_cache = T,
                  model.grenswijzigingen = "model.1",
                  PI_analytisch = F,
                  n_simulaties = 10,
                  min_gebruikers = 100,
                  voor_dashboard = F
  )
  # Zet het pad weer terug naar de originele working directory
  setwd(origineel_pad)
  expect_snapshot_file(path = file.path('..', '..', 'data', 'final','uitkomst_percentage_wmo_oh_gemeente.csv'),
                       name = 'simulatie_gemeente_oh.csv')
})
