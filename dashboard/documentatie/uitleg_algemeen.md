Het Wmo voorspelmodel helpt gemeenten bij het werken met de data die nodig zijn voor de lokale opgaven op het gebied van Wmo.
Het Wmo voorspelmodel geeft u tot 5 jaar vooruit inzicht in het aantal Wmo gebruikers per vastgestelde categorie. 
Daarnaast bestaat de mogelijkheid om zelf scenario analyses te maken.

Het model is met grote zorg samengesteld. Desondanks kan geen enkele garantie gegeven worden met betrekking tot de volledigheid, 
juistheid of actualiteit van de getoonde data. Deze data toepassing bevat geen enkele interpretatie. De informatie kan niet worden 
gebruikt in plaats van advies. Beslissingen op basis van deze informatie zijn voor uw eigen rekening en risico.