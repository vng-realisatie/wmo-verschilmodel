# dit is de UI voor het nieuwe dashboard


topbarPanel <- function(gemeenten){
  # Dit is het bovenste paneel 
  
  # Welke categorieen kunnen geselecteerd worden?
  alle_categorieen <- df.categorieen %>% filter(niveau=="wijk")

  div(
    class="table-row header",
    div(
      class="headerbox",
      div(
        class="linker_header",
        style="float: left;",
        actionLink(
          "logo",
          img(
            src = "logo_vng_gs.svg",
            alt = "VNG Logo", 
          ),
          style = "float: left; padding-top: 16px;"
        ),
        div(
          style = "overflow: hidden; padding-left: 24px;",
          h1("Wmo voorspelmodel"),
          p("Voorspelling Wmo gebruikers voor de komende 5 jaar")
        )
      ),
      div(
        class="rechter_header",
        style="float: right; display: flex; align-items: center; justify-content: right;",
        
        # selecteer een gemeente
        pickerInput(
          "gemeentecode",
          "Gemeente",
          choices=setNames(gemeenten$gemeentecode, gemeenten$gemeentenaam), 
          selected = NULL,
          options = list(
            `live-search` = TRUE,
            `live-search-normalize` = TRUE,
            `live-search-style` = 'contains',
            `select-on-tab` = TRUE,
            title = "Kies een gemeente"
          )
        ),
        
        # selecteer voorziening
        pickerInput(
          "categorie",
          "Voorziening (unieke gebruikers)",
          choices=setNames(
            alle_categorieen$categorie, 
            alle_categorieen$lange_naam
          ), 
          choicesOpt = list(
            disabled = !alle_categorieen$selectie
          ),
          selected = 'totaal',
          options = list(
            `select-on-tab` = TRUE,
            title = "Kies een voorziening"
          )
        )
      ),
      div(
        style="clear: both;"
      )
    )
  )
}


bottombarPanel <- function(gemeenten) {
  # Dit is het onderste paneel
  div(
    class="table-row body",
    div(
      class="table-cell body-content-outer-wrapper",
      div(
        class="body-content-inner-wrapper",
        id="scroll_div",
        div(
          class="body-content",
          
          conditionalPanel(
            "input.colofon_link % 2 == 1",
            style = "min-height: calc(100vh - 392px);",
            toelichtingsPanel(),
          ),
          
          conditionalPanel(
            "input.colofon_link % 2 == 0",
            style = "min-height: calc(100vh - 392px);",
            conditionalPanel(
              "input.gemeentecode == ''",
              startPanel(),
            ),
            
            conditionalPanel(
              "input.gemeentecode != ''",
              interactionPanel(),
              
              conditionalPanel(
                "input.scenarios % 2",
                div(
                  class = "scenario panel",
                  div(
                    class = "headerbox scenario",
                    div(
                      hr(),
                      p("Scenario's berekenen", style="font-weight: bold;"),
                      p("Bereken het effect op de voorspelling binnen de geselecteerde voorziening. 
                        U kunt meerdere indicatoren gelijktijdig wijzigen, 
                        deze worden complementair doorgevoerd. 
                        De ingegeven groei of afname wordt evenredig verdeeld over de 5 toekomstige jaren.
                        Voor verdere toelichting zie de handreiking, te downloaden via de Colofon."),
                      fluidRow(id='sliders')
                    )
                  )
                )
              ),
              
              indexPanel(gemeenten),
              
              grafiekPanel(),
              
              conditionalPanel(
                "input.gemeentecode != 'NL00'",
                wijkoverzichtPanel()
              )
            )
          ),

          colofonPanel()
          
          
        )
      )
    )
  )
}

startPanel <- function() {
  div(
    class = "box startPanel",
    leafletOutput("kaart_NL", height=900)
  )
}

toelichtingsPanel <- function() {
  div(
    div(
      class = "colofonbox knoppen",
      actionLink(
        "terug_van_toelichting",
        "Terug",
        icon = icon("arrow-left")
      )
    ),
    div(
      class = "box toelichtingsPanel",
      fluidRow(
        column(12, h1("Colofon & uitleg")),
        column(12, includeMarkdown("documentatie/uitleg_algemeen.md")),
      ),
      fluidRow(
        column(
          12,
          div(
            style = "display: table; width: 100%;",
            div(
              style = "display: table-cell; width: 50%; background-color: #E0F2F1; padding: 16px; border-right: 16px solid white;",
              div(
                div(
                  "Uitgebreide toelichting",
                  style = "font-weight: 600;"
                ),
                div(
                  "Voor uitgebreide toelichting op het gebruik van het Wmo
                  voorspelmodel kunt u de handreiking downloaden.
                  Hierin staat ook een toelichting op de technische achtergrond van
                  het model."
                ),
                a(
                  href='Handreiking Wmo voorspelmodel dec 23.pdf',
                  p(icon("download"), "Download handreiking (PDF)")
                ),
              ),
            ),
            div(
              style = "display: table-cell; width: 50%; background-color: #E0F2F1; padding: 16px;",
              div(
                div(
                  "Vragen of feedback?",
                  style = "font-weight: 600;"
                ),
                p('Mail naar ', a(href = 'mailto:wmovoorspelmodel@vng.nl', 'wmovoorspelmodel@vng.nl'), '.'),
              ),
              div(
                style = "height: 16px; background-color: white; margin: 16px -16px;"
              ),
              div(
                div(
                  "Gemeentelijke Monitor Sociaal Domein",
                  style = "font-weight: 600;"
                ),
                p(
                  'Meer informatie over het sociaal domein vindt u ', 
                  a(href = 'https://www.waarstaatjegemeente.nl/dashboard/dashboard/Gemeentelijke-Monitor-Sociaal-Domein/', 'hier'), 
                  '.'
                ),
              ),
              div(
                style = "height: 16px; background-color: white; margin: 16px -16px;"
              ),
              div(
                div(
                  "Wmo-gemeentevergelijker",
                  style = "font-weight: 600;"
                ),
                p(
                  'Vergelijkbare gemeenten in het sociaal domein vindt u ', 
                  a(href = 'https://wmo-gemeentevergelijker.waarstaatjegemeente.nl/', 'hier', target="_blank", rel="noopener noreferrer"), 
                  '.'
                ),
              ),
            )
          )
        )
      ),
      # fluidRow(
      #   column(
      #     6, 
      #     div(
      #       div(
      #         "Uitgebreide toelichting",
      #         style = "font-weight: 600;"
      #       ),
      #       div(
      #         "Voor uitgebreide toelichting op het gebruik van het Wmo 
      #         voorspelmodel kunt u de handreiking downloaden. 
      #         Hierin staat ook een toelichting op de technische achtergrond van 
      #         het model."
      #       ),
      #       a(
      #         href='Handreiking Wmo voorspelmodel december 2021 PDF.pdf',  
      #         p(icon("download"), "Download handreiking (PDF)")
      #       ),
      #       style = "background-color: #E0F2F1; padding: 16px;"  
      #     ),
      #     style = "display: table-cell;"
      #   ),
      #   column(
      #     6,
      #     div(
      #       div(
      #         "Vragen of feedback?",
      #         style = "font-weight: 600;"
      #       ),
      #       p('Mail naar ', a(href = 'mailto:wmovoorspelmodel@vng.nl', 'wmovoorspelmodel@vng.nl'), '.'),
      #       style = "background-color: #E0F2F1; padding: 16px; margin-bottom: 16px;"
      #     ),
      #     div(
      #       div(
      #         "Gemeentelijke Monitor Sociaal Domein",
      #         style = "font-weight: 600;"
      #       ),
      #       p('Mail naar ', a(href = 'mailto:wmovoorspelmodel@vng.nl', 'wmovoorspelmodel@vng.nl'), '.'),
      #       style = "background-color: #E0F2F1; padding: 16px;"
      #     ),
      #     style = "display: table-cell;"
      #   ),
      #   style = "display: table;"
      # ),
      fluidRow(
        column(12, includeMarkdown("documentatie/uitleg_ontwikkeling.md")),
        column(12, h2("Licentie")),
        column(12, includeMarkdown('documentatie/uitleg_licentie.md'))
      )
    )
    
  )
    
}


# Panel met interacties op hoofdpaneel (scenario berekenen en downloads)
interactionPanel <- function() {
  div(
    class = "interactionPanel",
    div(
      style = "float: left;",
      # scenarios
      actionLink(
        "scenarios",
        "Scenario berekenen",
       icon = icon("chevron-down"),
       style = "font-weight: 600; text-decoration: none;",
      )
    ),
    div(
      style = "float: right;",
      actionLink(
        "download_gegevens",
        "Download gegevens",
        icon = icon("download"),
        style = "font-weight: 600; text-decoration: none;",
      )
    ),
    div(
      style = "clear: both;"
    )
  )
}


indexPanel <- function(gemeenten) {
  div(
    class = "box indexPanel",
    # index tabel
    fluidRow(
      column(
        width = 4,
        div(
          h3("Voorspelling Wmo gebruikers"),
          hidden(
            actionLink(
              inputId="warning_indexPanel",
              label="",
              icon=icon("warning"),
              style = "padding-left: 16px"
            )
          ),
          style = "display: inline-flex;"
        ),
        div(class="small_text", textOutput("gekozen_gemeente"))
      ),
      column(
        width = 8,
        align = "right",
        div(
          id = "vergelijk",
          style="float: right; display: flex; align-items: center; justify-content: right;",
          div(
            icon("external-link-alt"),
            HTML('<a href="https://wmo-gemeentevergelijker.waarstaatjegemeente.nl/" target="_blank" rel="noopener noreferrer", style="color: inherit; text-decoration:underline;">Wmo-gemeentevergelijker</a>'),
            style = "margin: 0 0 15px 24px;"
          ),
          actionLink(
            "verwijder_vergelijk_gemeente",
            "Stop vergelijking",
            style = "margin: 0 0 15px 24px;"
          ),
          pickerInput(
            "vergelijkgemeentecode",
            NULL,
            choices=setNames(
              gemeenten$gemeentecode, 
              gemeenten$gemeentenaam
            ), 
            selected = NULL,
            options = list(
              `live-search` = TRUE,
              `live-search-normalize` = TRUE,
              `live-search-style` = 'contains',
              `select-on-tab` = TRUE,
              title = "Vergelijk met gemeente"
            )
          )
        )
      ),
    ),
    fluidRow(
      column(
        width = 12,
        DT::dataTableOutput("voorspelling_wmo_gebruikers")
      )
    ),
    conditionalPanel(
      "output.empty_predictions == true",
      fluidRow(
        column(
          width = 12,
          div(
            style = "margin-top: 10px !important",
            class = "alert alert-warning",
            "Voor deze gemeente zijn van het laatste jaar geen data bekend over Wmo gebruikers; daarom kunnen de voorspellingen en grafieken niet worden weergegeven. Gemeenten leveren deze gegevens zelf halfjaarlijks aan via een uitvraag van de Gemeentelijke Monitor Sociaal Domein."
          )
        )
      )
    ),
    # laat subcategorieën zien in geval van hulpmiddelen en diensten
    conditionalPanel(
      "input.categorie == 'hulpmiddel'",
      fluidRow(
        column(
          width = 12,
          actionLink(
            "subcategorieen",
            "Toon subvoorzieningen (alleen gemeenteniveau)",
            icon = icon("chevron-down"),
            style = "font-weight: 600; text-decoration: none;",
          )
        )
      )
    )
  )
}


grafiekPanel <- function() {
  div(
    class = "box grafiekPanel",
    # Grafiek lijst
    div(
      class = "grafiek_links",
      style = "display: inline-block;",
      div(
        div(
          h3("Prognose Wmo gebruikers per jaar"),
          hidden(
            actionLink(
              inputId="warning_grafiekPanel",
              label="",
              icon=icon("warning"),
              style = "padding-left: 16px"
            )
          ),
          style = "display: inline-flex;"
        ),        
        p(
          class="small_text", 
          "Selecteer wijken in het wijkoverzicht om ze toe te voegen"
        )
      )
    ),
    div(
      class = "overzicht_rechts",
      style = "display: inline-block; text-align: right; float: right;",
      div(
        style = "display: inline-block; margin-left: 24px;",
        checkboxInput(
          inputId = "betrouwbaarheid",
          label = sprintf("Betrouwbaarheidsinterval (%.0f%%)", 100*alpha),
          value = FALSE
        )
      ),
      div(
        style = "display: inline-block; margin-left: 24px;",
        radioGroupButtons(
          inputId = "toggle_grafiek_lijst",
          label = NULL, 
          choices = c("Grafiek", "Lijst"),
          status = "primary"
        )
      )
    ),
    conditionalPanel(
      "input.toggle_grafiek_lijst=='Grafiek'",
      fluidRow(
        column(
          width = 12,
          plotOutput("grafiek_indexcijfers")
        )
      )
    ),
    
    conditionalPanel(
      "input.toggle_grafiek_lijst=='Lijst'",
      fluidRow(
        column(
          width = 12,
          DTOutput("lijst_indexcijfers")
        )
      )
    )
  )  
}


wijkoverzichtPanel <- function() {
  div(
    class = "box wijkoverzichtPanel",
    # Kaart lijst
    div(
      class = "overzicht_links",
      style = "display: inline-block;",
      div(
        h3("Wijkoverzicht"),
        p(
          class="small_text",
          "Selecteer wijken in de lijst om ze toe te voegen aan de grafiek hierboven"
        )
      )
    ),
    div(
      class = "overzicht_rechts",
      style = "display: inline-block; text-align: right; float: right;",
      div(
        style = "display: inline-block; margin-left: 24px;",
        actionLink(
          "voeg_wijkgroep_toe", 
          "Voeg wijkgroep toe",
          icon = icon("plus"),
          style = "font-weight: 600; text-decoration: none;"
        ),
      ),
      div(
        style = "display: inline-block;",
        hidden(
          actionLink(
            "beheer_wijkgroep", 
            "Beheer wijkgroepen",
            style = "font-weight: 600; text-decoration: none; margin-left: 24px;"
          )
        ),
      ),
      div(
        style = "display: inline-block; margin-left: 24px;",
        plus_min_knop("jaar"),
      ),
      div(
        style = "display: inline-block; margin-left: 24px;",
        align = "right",
        radioGroupButtons(
          inputId = "toggle_kaart_lijst",
          label = NULL, 
          choices = c("Lijst", "Kaart"),
          status = "primary"
        )
      )
    ),
    
    conditionalPanel(
      "input.toggle_kaart_lijst=='Kaart'",
      fluidRow(
        column(
          width = 12,
          leafletOutput("kaart_indexcijfers")
        )
      )
    ),
    
    conditionalPanel(
      "input.toggle_kaart_lijst=='Lijst'",
      fluidRow(
        column(
          width = 12,
          DT::DTOutput("wijkoverzicht")
        )
      )
    ),

  )
  
}


colofonPanel <- function() {
  div(
    class = "colofonPanel",
    style = "margin-top: 80px;",
    div(
      class = "colofonbox",
      style = "justify-content: center; display: flex; align-items: center;",
      div(
        img(
          src = "logo_vng_gs.svg",
          alt = "VNG Logo", 
          style = "height: 32px; margin-right: 8px;"
        )
      ),
      div(
        span("Ontwikkeld in opdracht van VNG")
      ),
      div(
        actionLink(
          "colofon_link",
          "Colofon & uitleg",          
          style="margin-left: 40px; color: white; text-decoration: underline;",
          icon = icon("info-circle")
        )
      )
    )
  )
}



plus_min_knop <- function(inputId, label=NULL) {
  list(
    div(
      div(
        actionButton(
          paste0("min_knop_", inputId), 
          NULL,
          icon = icon("minus"),
          class="btn btn-default action-button plusmin-button min-btn", 
        ),
        style="display:inline-block; margin-right: -5px;"
      ),
      div(
        tagAppendAttributes(
          textOutput(inputId),
          class="plusmin-button text-panel",
          style="padding: 0 16px; line-height: 32px;"
        ), 
        style="display:inline-block; margin-right: -5px;"
      ),
      div(
        actionButton(
          paste0("plus_knop_", inputId), 
          NULL,
          icon = icon("plus"),
          class="btn btn-default action-button plusmin-button plus-btn", 
        ), 
        style="display:inline-block; margin-right: -5px;"
      ),
    )
  )
}
